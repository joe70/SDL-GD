/*
 * TextureManager.h
 *
 *  Created on: 29 abr. 2018
 *      Author: joe
 */

#ifndef INCLUDE_TEXTUREMANAGER_H_
#define INCLUDE_TEXTUREMANAGER_H_

#include <SDL.h>
#include <vector>
#include <map>
#include <string>

class TextureManager {
private:
	TextureManager();
	TextureManager(const TextureManager&); // si trata de usarlo va a avisar
	static TextureManager* s_pInstance;
public:
	static TextureManager* Instance() {
		if(s_pInstance == nullptr) {
			s_pInstance = new TextureManager();
			return s_pInstance;
		}
		return s_pInstance;
	}
	bool load(std::string fileName, std::string id, SDL_Renderer* pRenderer);
	void draw (std::string id, int x, int y, int width, int height,\
			SDL_Renderer* pRenderer, SDL_RendererFlip flip=SDL_FLIP_NONE);
	void drawFrame(std::string id, int x, int y, int width, int height,\
			int currentRow, int currentFrame, SDL_Renderer* pRenderer,\
			SDL_RendererFlip flip=SDL_FLIP_NONE);

	std::map<std::string, SDL_Texture*> m_textureMap;
	void clearFromTextureMap(std::string id) { m_textureMap.erase(id); }
	~TextureManager();
};
typedef TextureManager TheTextureManager;

#endif /* INCLUDE_TEXTUREMANAGER_H_ */
