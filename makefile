EXEC := main
DIRSRC := src/
DIROBJ := obj/
DIRHEA := include/

#Compilador
CC := g++
#Flags de compilación
CXXFLAGS := -I/usr/include/SDL2 -Wall -I$(DIRHEA) -D_REENTRANT -std=c++11
#Flags de enlazado
LDFLAGS := -L/usr/lib/x86_64-linux-gnu -lSDL2 -lSDL2_image -lstdc++;

ifeq ($(mode), release)
	CXXFLAGS += -02 -D_RELEASE
else
	CXXFLAGS += -ggdb -D_DEBUG
	mode := debug
endif
# Obtencion automática de la lista de objetos a compilar ------
OBJS := $(subst $(DIRSRC), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)*.cpp)))

.PHONY: all clean

all: info $(EXEC)

info:
	@echo '---------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '	   (Please, call "make" with [mode=debug|release])'
	@echo '---------------------------------------------------'

# Enlazado -----------------------------------------
$(EXEC): $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS)

# Compilación --------------------------------------
$(DIROBJ)%.o: $(DIRSRC)%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Limpieza de temporales ---------------------------
clean:
	rm -f $(EXEC) *~ $(DIROBJ)* $(DIRSRC)*~ $(DIRHEA)*~