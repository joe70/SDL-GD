/*
 * MenuState.cpp
 *
 *  Created on: 21 jul. 2018
 *      Author: joe
 */

#include "MenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
#include <iostream>

const std::string MenuState::s_menuID="MENU";

void MenuState::update() {
	for (unsigned int i = 0; i < m_gameObjects.size(); ++i) {
		m_gameObjects[i]->update();
	}
}

void MenuState::render() {
	for (unsigned int i = 0; i < m_gameObjects.size(); ++i) {
		m_gameObjects[i]->draw();
	}
}

bool MenuState::onEnter() {
	std::cout << "Entering MenuState\n";
	if (!TheTextureManager::Instance()->load("assets/button.png", "playbutton", \
			TheGame::Instance()->getRenderer())) {
		return false;
	}
	if (!TheTextureManager::Instance()->load("assets/exit.png", "exitbutton", \
			TheGame::Instance()->getRenderer())) {
		return false;
	}
	GameObject* button1 = new MenuButton(new LoaderParams (100, 100, 400, 100, "playbutton"), s_menutToPlay);
	GameObject* button2 = new MenuButton(new LoaderParams (100, 300, 400, 100, "exitbutton"), s_exitFromMenu);

	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	return true;
}

bool MenuState::onExit() {
	for (unsigned int i = 0; i < m_gameObjects.size(); ++i) {
		m_gameObjects[i]->clean();
	}
	m_gameObjects.clear(); // clear es una funcion de los vectores
	TheTextureManager::Instance()->clearFromTextureMap("playbutton");
	TheTextureManager::Instance()->clearFromTextureMap("exitbutton");
	std::cout << "exiting MenuState\n";
	return true;
}

void MenuState::s_menutToPlay() {
	std::cout << "Play button clicked\n";
	TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void MenuState::s_exitFromMenu(){
	std::cout << "Exit button clicked\n";
	TheGame::Instance()->quit();
}
