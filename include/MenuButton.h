/*
 * MenuButton.h
 *
 *  Created on: 22 jul. 2018
 *      Author: joe
 */

#ifndef INCLUDE_MENUBUTTON_H_
#define INCLUDE_MENUBUTTON_H_
#include "SDLGameObject.h"

class MenuButton : public SDLGameObject {
public:
	MenuButton(const LoaderParams* pParams, void(*callback)());

	virtual void draw();
	virtual void update();
	virtual void clean();
	virtual ~MenuButton() {}
private:
	enum button_state {
		MOUSE_OUT=0,
		MOUSE_OVER=1,
		CLICKED=2
	};
	void (*m_callback)();
	bool m_bReleased;
};
#endif /* INCLUDE_MENUBUTTON_H_ */
