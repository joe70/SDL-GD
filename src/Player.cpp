/*
 * Player.cpp
 *
 *  Created on: 4 may. 2018
 *      Author: joe
 */
#include <SDL.h>
#include <iostream>
#include "Player.h"
#include "InputHandler.h"

Player::Player(const LoaderParams* pParams) : SDLGameObject(pParams) {}

Player::~Player() {
	std::cout << "En destructor de Player \n";
}

void Player::handleInput() {
	if(TheInputHandler::Instance()->joysticksInitialised()) {
		if( TheInputHandler::Instance()->xvalue(0,1) > 0 ||
			TheInputHandler::Instance()->xvalue(0,1) < 0 ) {
			m_velocity.setX(1 * TheInputHandler::Instance()->xvalue(0,1));
		}
		if( TheInputHandler::Instance()->yvalue(0,1) > 0 ||
			TheInputHandler::Instance()->yvalue(0,1) < 0 ) {
			m_velocity.setY(1 * TheInputHandler::Instance()->yvalue(0,1));
		}
		if( TheInputHandler::Instance()->xvalue(0,2) > 0 ||
			TheInputHandler::Instance()->xvalue(0,2) < 0 ) {
			m_velocity.setX(1 * TheInputHandler::Instance()->xvalue(0,2));
		}
		if ( TheInputHandler::Instance()->yvalue(0,2) > 0 ||
			 TheInputHandler::Instance()->yvalue(0,2) < 0) {
			 m_velocity.setY(1 * TheInputHandler::Instance()->yvalue(0,2));
		}
		// comprueba si se ha pulsado el botón3 del joystick
		if (TheInputHandler::Instance()->getButtonState(0,3)) {
			m_velocity.setX(1);
		}
	}
	// Utilizamos el botón izquierdo del ratón para establecer una velocidad
	if (TheInputHandler::Instance()->getMouseButtonState(LEFT)) {
		m_velocity.setX(3);
		std::cout << "Botón izquierdo de ratón pulsado" << std::endl;
	}


	if (TheInputHandler::Instance()->getMouseButtonState(RIGHT)) {
		m_acceleration.setX(3);
		std::cout << "Botón derecho de ratón pulsado" << std::endl;
	}

	// Captura de la posición de ratón y llevamos el personaje hacia allí
	//Vector2D* vec=TheInputHandler::Instance()->getMousePosition();
	//m_velocity=(*vec - m_position)/100; // (/2 drag)

	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT)) {
		m_velocity.setX(2);
		std::cout << "Botón derecho pulsado" << std::endl;
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT)) {
		m_velocity.setX(-2);
		std::cout << "Botón izquierdo pulsado" << std::endl;
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP)) {
		m_velocity.setY(-2);
		std::cout << "Botón arriba pulsado" << std::endl;
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN)) {
		m_velocity.setY(2);
		std::cout << "Botón abajo pulsado" << std::endl;
	}
}


void Player::draw() {
	SDLGameObject::draw();
}

void Player::update() {
	//m_position.setX(m_position.getX()+1);
	//m_position.setY(m_position.getY());
	m_velocity.setX(0);
	m_acceleration.setY(0);
	handleInput();
	m_currentFrame=int(((SDL_GetTicks() / 100) % 6));
	// std::cout << "current frame: " << m_currentFrame << std::endl;
	// cout actualización de posición en función de
	// velocidad y aceleración
	std::cout << "Posición de player:" << std::endl;
	SDLGameObject::update();
}

void Player::clean() {}
