/*
 * Game.h
 *
 *  Created on: 29 abr. 2018
 *      Author: joe
 */

#ifndef INCLUDE_GAME_H_
#define INCLUDE_GAME_H_
#include <SDL.h>
#include <vector>
#include "GameObject.h"
#include "GameStateMachine.h"

class Game {
private:
	Game();
	Game(const Game&);
	static Game* s_pInstance;
	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;

	// std::vector<GameObject*> m_gameObjects;

	int m_currentFrame;
	bool m_bRunning;

	GameStateMachine* m_pGameStateMachine;

public:
	static Game* Instance() {
		if(s_pInstance == nullptr)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}
		return s_pInstance;
	}
	bool init(const char* title, int xpos, int ypos, int width,\
			int height, bool fullscreen);
	void render();
	void update();
	void handleEvents();
	void clean();
	void quit();
	~Game();
	SDL_Renderer* getRenderer() const { return m_pRenderer; }
	GameStateMachine* getStateMachine() const {
		return m_pGameStateMachine;
	}

	bool running() { return m_bRunning; }
};
typedef Game TheGame;

#endif /* INCLUDE_GAME_H_ */
