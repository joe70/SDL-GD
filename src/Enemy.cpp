/*
 * Enemy.cpp
 *
 *  Created on: 4 may. 2018
 *      Author: joe
 */
#include "Enemy.h"
#include <SDL.h>
#include <iostream>

Enemy::Enemy(const LoaderParams* pParams) : SDLGameObject(pParams) {
}

Enemy::~Enemy() {
	std::cout << "en destructor de Enemy \n";
}

void Enemy::draw() {
	SDLGameObject::draw();
}

void Enemy::update() {
	//m_position.setX(m_position.getX()+1);
	//m_position.setY(m_position.getY());
	m_currentFrame=int(((SDL_GetTicks() / 100) % 6));
	m_velocity.setX(1);
	m_acceleration.setX(1);
	SDLGameObject::update();
}

void Enemy::clean() {}
