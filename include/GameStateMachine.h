/*
 * GameStateMachine.h
 *
 *  Created on: 21 jul. 2018
 *      Author: joe
 */

#ifndef INCLUDE_GAMESTATEMACHINE_H_
#define INCLUDE_GAMESTATEMACHINE_H_

#include "GameState.h"
#include <vector>

class GameStateMachine {
public:
	GameStateMachine();
	void pushState(GameState* pState);
	void changeState(GameState* pState);
	void popState();

	void update();
	void render();

	~GameStateMachine();

private:
	std::vector<GameState*> m_gameStates;
};
#endif /* INCLUDE_GAMESTATEMACHINE_H_ */
