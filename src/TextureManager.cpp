/*
 * TextureManager.cpp
 *
 *  Created on: 28 mar. 2018
 *      Author: joe
 */
#include <iostream>
#include <SDL_image.h>
#include "TextureManager.h"

// Inicializamos el miembro estático
TextureManager* TextureManager::s_pInstance=nullptr;

TextureManager::TextureManager() {}

TextureManager::~TextureManager() {
	std::cout << "en el destructor  de TextureManager()\n";

	// destruimos las texturas del mapa de texturas
	std::map<std::string,SDL_Texture*>::iterator it;
	for (it=m_textureMap.begin(); it!=m_textureMap.end(); ++it) {
		SDL_DestroyTexture(it->second);
		m_textureMap.erase(it);
	}
	// liberamos la memoria
	delete s_pInstance;
	s_pInstance=nullptr;
}

// Loading a texture
bool TextureManager::load(std::string fileName, std::string id,\
		SDL_Renderer* pRenderer)
{
	// Cargamos la imagen
	SDL_Surface* pTempSurface=IMG_Load(fileName.c_str());
	if ( pTempSurface == NULL )
	{
		std::cout << "SDL_LoadBMP fail: " << SDL_GetError() << std::endl;
		return false;
	}
	std::cout << "loading image\n";

	SDL_Texture* pTexture = SDL_CreateTextureFromSurface(pRenderer, pTempSurface);
	if ( pTexture == NULL )
	{
		std::cout << "SDL_CreateTextureFromSurface: " << SDL_GetError() << std::endl;
		return false;
	}
	std::cout << "initialize a texture\n";

	// Liberamos la SDL temporal
	SDL_FreeSurface(pTempSurface);

	// everything went ok, add the texture to our list
	if(pTexture != 0)
	{
		m_textureMap[id]=pTexture;
		return true;
	}

	// reaching here means something went wrong
	return false;
}

// Drawing a texture accessing with the id
void TextureManager::draw(std::string id, int x, int y, int width, int height,\
		SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;

	srcRect.x=0;
	srcRect.y=0;
	srcRect.w=destRect.w=width;
	srcRect.h=destRect.h=height;

	destRect.x=x;
	destRect.y=y;
	if ( SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip) ) {
		std::cout << "RenderCopyEx() failure: " << __FUNCTION__ << __LINE__ << __FILE__ <<\
				std::endl;
		std::cout << "SDL_RenderCopyEx() failure: " << SDL_GetError() << std::endl;
	}
}

void TextureManager::drawFrame(std::string id, int x, int y, int width, int height,\
		int currentRow, int currentFrame, SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
	SDL_Rect srcRect;
	SDL_Rect destRect;
	srcRect.x=width*currentFrame;
	srcRect.y=height*(currentRow-1);
	srcRect.w=destRect.w=width;
	srcRect.h=destRect.h=height;
	destRect.x=x;
	destRect.y=y;

	if ( SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip) ) {
		std::cout << "RenderCopyEx() failure: " << __FUNCTION__ << __LINE__ << __FILE__ << \
				std::endl;
		std::cout << "SDL_RenderCopyEx() failure: " << SDL_GetError() << std::endl;
	}
}

