/*
 * InputHandler.h
 *
 *  Created on: 8 jun. 2018
 *      Author: joe
 */

#ifndef INCLUDE_INPUTHANDLER_H_
#define INCLUDE_INPUTHANDLER_H_

#include <SDL.h>
#include <vector>
#include "Vector2D.h"

const int m_joystickDeadZone=10000;

enum mouse_buttons {
	LEFT=0,
	MIDDLE=1,
	RIGHT=2
};

class InputHandler {
private:
	InputHandler();
	InputHandler(const InputHandler&); // constructor de copia privado ver Thinking C++ pag 294
	~InputHandler();

	// Control del número de joysticks
	std::vector<SDL_Joystick*> m_joysticks;
	// control de movimiento sticks analógicos
	std::vector<std::pair<Vector2D*, Vector2D*>> m_joystickValues;
	// array de valores booleanos para cada botón del controlador del joystick
	std::vector<std::vector<bool>> m_buttonStates;
	bool m_bJoysticksInitialised;

	// Almacenar botones del ratón
	std::vector<bool> m_mouseButtonStates;

	// Control de la posición del ratón
	Vector2D* m_mousePosition;

	// Puntero al estado del teclado
	const Uint8* m_keystates;


	// private functions to handle different event types
	// handle keyboard events
	void onKeyDown();
	void onKeyUp();

	// handle mouse events
	void onMouseMove(SDL_Event& event);
	void onMouseButtonDown(SDL_Event& event);
	void onMouseButtonUp(SDL_Event& event);

	// handle joysticks events
	void onJoystickAxisMove(SDL_Event& event);
	void onJoystickButtonDown(SDL_Event& event);
	void onJoystickButtonUp(SDL_Event& event);



	static InputHandler* s_pInstance;
public:
	static InputHandler* Instance() {
		if(s_pInstance == nullptr) {
			s_pInstance = new InputHandler();
		}
		return s_pInstance;
	}

	// inicializar joysticks
	void initialiseJoysticks();
	bool joysticksInitialised() const {
		return m_bJoysticksInitialised;
	}

	void reset();

	// captar valores del array de pares
	int xvalue(int joy, int stick) const;
	int yvalue(int joy, int stick) const;

	// captar el estado de los botones del joystick
	bool getButtonState(int joy, int buttonNumber) const;

	// Acceso a estado de botones del ratón
	bool getMouseButtonState(int buttonNumber) const;

	// Acceso a la posición del ratón
	Vector2D* getMousePosition() const {
		return m_mousePosition;
	}

	// Evaluar pulsación de teclas
	bool isKeyDown(SDL_Scancode key) const;


	void update();
	void clean();
};

typedef InputHandler TheInputHandler;

#endif /* INCLUDE_INPUTHANDLER_H_ */
