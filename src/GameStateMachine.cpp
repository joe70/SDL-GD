/*
 * GameStateMachine.cpp
 *
 *  Created on: 21 jul. 2018
 *      Author: joe
 */

#include "GameStateMachine.h"

GameStateMachine::GameStateMachine() {
}

void GameStateMachine::pushState(GameState *pState) {
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}

void GameStateMachine::popState() {
	if(!m_gameStates.empty()) { // si está vacío no entra
		if (m_gameStates.back()->onExit()) {
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
}

void GameStateMachine::changeState(GameState* pState) {
	if (!m_gameStates.empty()) {
		if(m_gameStates.back()->getStateID() == pState->getStateID()) { // es el mismo
			return; // do nothing
		}
		if (m_gameStates.back()->onExit()) {
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
	m_gameStates.push_back(pState);

	// initialise it
	m_gameStates.back()->onEnter();
}

void GameStateMachine::update() {
	if (!m_gameStates.empty()) {
		m_gameStates.back()->update();
	}
}

void GameStateMachine::render() {
	if (!m_gameStates.empty()) {
		m_gameStates.back()->render();
	}
}

GameStateMachine::~GameStateMachine() { // @suppress("Member declaration not found")
	for(std::vector<GameState*>::size_type i=0; i!=m_gameStates.size(); i++)
		delete m_gameStates[i];
}

