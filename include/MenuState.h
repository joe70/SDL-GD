/*
 * MenuState.h
 *
 *  Created on: 21 jul. 2018
 *      Author: joe
 */

#ifndef INCLUDE_MENUSTATE_H_
#define INCLUDE_MENUSTATE_H_

#include "GameState.h"
#include "GameObject.h"
#include <vector>

class MenuState : public GameState {
public:
	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual ~MenuState() {};

	virtual std::string getStateID() const { return s_menuID; }

private:
	static const std::string s_menuID;
	std::vector<GameObject*> m_gameObjects;

	static void s_menutToPlay();
	static void s_exitFromMenu();
};



#endif /* INCLUDE_MENUSTATE_H_ */
