/*
 * GameState.h
 *
 *  Created on: 21 jul. 2018
 *      Author: joe
 */

#ifndef INCLUDE_GAMESTATE_H_
#define INCLUDE_GAMESTATE_H_

#include <string>

class GameState {
public:
	virtual void update()=0;
	virtual void render()=0;

	virtual bool onEnter()=0;
	virtual bool onExit()=0;

	virtual std::string getStateID() const=0;
	virtual ~GameState() {}
};

#endif /* INCLUDE_GAMESTATE_H_ */
