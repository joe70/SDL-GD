/*
 * Enemy.h
 *
 *  Created on: 4 may. 2018
 *      Author: joe
 */

#ifndef INCLUDE_ENEMY_H_
#define INCLUDE_ENEMY_H_
#include "SDLGameObject.h"

class Enemy : public SDLGameObject {
public:
	Enemy(const LoaderParams* pParams);
	virtual ~Enemy();

	virtual void draw();
	virtual void update();
	virtual void clean();
};
#endif /* INCLUDE_ENEMY_H_ */
