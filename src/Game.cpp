/*
 * Game.cpp
 *
 *  Created on: 29 abr. 2018
 *      Author: joe
 */
#include "Game.h"
#include "TextureManager.h"
#include "Player.h"
#include "Enemy.h"
#include "InputHandler.h"
#include "MenuState.h"
#include "PlayState.h"
#include <iostream>

Game* Game::s_pInstance = nullptr;

Game::Game() {
	m_pWindow=nullptr;
	m_pRenderer=nullptr;
	m_bRunning=false;
	m_currentFrame=0;
	m_pGameStateMachine=new GameStateMachine();
}

Game::~Game() {
	std::cout << "en destructor de Game\n";
	delete s_pInstance;
	s_pInstance=nullptr;
}

bool Game::init(const char* title, int xpos, int ypos, int width,\
		int height, bool fullscreen) {

	int flags=0;

	if ( fullscreen )
		flags=SDL_WINDOW_FULLSCREEN;
	else
		flags=SDL_WINDOW_SHOWN;

	// attempt to initialize SDL
	if ( SDL_Init(SDL_INIT_EVERYTHING) ) {
		std::cout << "SDL init fail: " << SDL_GetError() << std::endl;
	}
	std::cout << "SDL init success\n";

	// init the window
	if ( ( m_pWindow = SDL_CreateWindow(title, xpos, ypos, width,\
			height, flags) ) == NULL) {
		std::cout << "window init fail: " << SDL_GetError() << std::endl;
		return false;
	}
	std::cout << "window creation success\n";

	if ( ( m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0) ) == NULL ) {
		std::cout << "renderer init fail: " << SDL_GetError() << std::endl;
		return false;
	}
	std::cout << "renderer creation success\n";

	if ( SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 255, 255) ) {
		std::cout << "drawing fail: " << SDL_GetError() << std::endl;
		return false;
	}
	std::cout << "drawing success\n";

	/* if (!( TheTextureManager::Instance()->load("assets/animate-alpha.png", "animate", m_pRenderer))) {
		std::cout << "Texture::load() failure" << std::endl;
		return false;
	} */
	std::cout << "loading image\n";
	m_pGameStateMachine->changeState(new MenuState());
	/* m_gameObjects.push_back(new Player(new LoaderParams(100, 100, 128, 82, "animate")));
	m_gameObjects.push_back(new Enemy(new LoaderParams(300, 300, 128, 82, "animate"))); */

	TheInputHandler::Instance()->initialiseJoysticks();

	std::cout << "init success\n";
	m_bRunning=true; // everything inited successfully

	// start the main loop
	return true;
}
void Game::render()
{
	// Limpia el render con el color de drawing (línea 51)
	if ( SDL_RenderClear(m_pRenderer) )
		std::cout << "clear render fail: " << SDL_GetError() << std::endl;

	m_pGameStateMachine->render();

	/*
	// loop through and update our objects
	for(std::vector<GameObject*>::size_type i=0; i!=m_gameObjects.size(); i++) {
		m_gameObjects[i]->draw();
	}
	*/
	SDL_RenderPresent(m_pRenderer); // draw to the screen
}
void Game::handleEvents()
{
	TheInputHandler::Instance()->update();

	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RETURN)) {
		m_pGameStateMachine->changeState(new PlayState());
	}
}

void Game::update() {
	/*
	// loop through and update our objects
	for(std::vector<GameObject*>::size_type i=0; i!=m_gameObjects.size(); i++) {
		m_gameObjects[i]->update();
	}
	*/
	m_pGameStateMachine->update();
}

void Game::clean()
{
	m_bRunning=false; // everything inited successfully
}

void Game::quit() {
	std::cout << "cleaning game Game::quit()\n";

	TheInputHandler::Instance()->clean();

	/* for(std::vector<GameObject*>::size_type i=0; i!=m_gameObjects.size(); i++) {
			delete m_gameObjects[i];
	 }*/
	delete m_pGameStateMachine;

	//TheTextureManager::Instance()->~TextureManager();
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);

	SDL_Quit();
}
