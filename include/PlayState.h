/*
 * PlayState.h
 *
 *  Created on: 21 jul. 2018
 *      Author: joe
 */

#ifndef INCLUDE_PLAYSTATE_H_
#define INCLUDE_PLAYSTATE_H_

#include "GameState.h"

class PlayState : public GameState {
public:
	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual ~PlayState() {}

	virtual std::string getStateID() const { return s_playID; }

private:
	static const std::string s_playID;
};
#endif /* INCLUDE_PLAYSTATE_H_ */
