/*
 * Player.h
 *
 *  Created on: 4 may. 2018
 *      Author: joe
 */

#ifndef INCLUDE_PLAYER_H_
#define INCLUDE_PLAYER_H_
#include "SDLGameObject.h"

class Player : public SDLGameObject {
public:
	Player(const LoaderParams* pParams);
	virtual ~Player();

	void handleInput();
	virtual void draw();
	virtual void update();
	virtual void clean();
};
#endif /* INCLUDE_PLAYER_H_ */
