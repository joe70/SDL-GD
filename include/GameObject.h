/*
 * GameObject.h
 *
 *  Created on: 29 abr. 2018
 *      Author: joe
 */

#ifndef INCLUDE_GAMEOBJECT_H_
#define INCLUDE_GAMEOBJECT_H_
#include "LoaderParams.h"

class GameObject {
public:
	virtual void draw()=0;
	virtual void update()=0;
	virtual void clean()=0;
	virtual ~GameObject() {}
protected:
	GameObject(const LoaderParams* pParams) {}

};
#endif /* INCLUDE_GAMEOBJECT_H_ */
